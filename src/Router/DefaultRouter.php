<?php

declare(strict_types=1);

namespace OptiFrame\Http\Router;

use OptiFrame\Http\DTO\Request;

class DefaultRouter extends AbstractRouter
{
    public function __construct(Request $request)
    {
        
    }

    public function getRoutes(): array
    {
        return [
            '/\/api\/user\/\d+$/' => [
                'controller' => \OptiFrame\Http\Controller\DefaultController::class,
                'method' => 'getNotFound',
                'path' => '/api/user/{userId}'
            ],
            '/\/api\/user\/\d+\/adress\/\d+$/' => [
                'controller' => \OptiFrame\Http\Controller\DefaultController::class,
                'method' => 'getNotFound',
                'path' => '/api/user/{userId}/adress/{adress}'
            ],
            '/\/api\/user\/\d+\/adress\/\d+\/street\/[a-zA-Z]+$/' => [
                'controller' => \OptiFrame\Http\Controller\DefaultController::class,
                'method' => 'getNotFound',
                'path' => '/api/user/{userId}/adress/{adress}/street/{street}'
            ]
        ];
    }
}