<?php

declare(strict_types=1);

namespace OptiFrame\Http\Router;

abstract class AbstractRouter
{
    abstract public function getRoutes(): array;
}