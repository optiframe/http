<?php

declare(strict_types=1);

namespace OptiFrame\Http\Router;

use OptiFrame\Http\Controller\AbstractController;
use OptiFrame\Http\Controller\DefaultController;
use OptiFrame\Http\DTO\Request;
use OptiFrame\Library\Handler\HandleTrait;
use OptiFrame\Library\Parser\Placeholder;

class Router
{
    use HandleTrait;

    /** @var AbstractRouter[] */
    private array $routers;
    private AbstractController $controller;
    private string $method;
    private array $arguments;

    private const ROUTERS = \APP_PATH . '/config/routers.inc';

    public function __construct(
        private Request $request
    ) {
        $this->routers = include self::ROUTERS;
        $this->matchRoute();
    }

    public function getResponseArguments(): array
    {
        return [
            $this->controller ?? new DefaultController($this->request),
            $this->method ?? 'getNotFound',
            $this->arguments ?? []
        ];
    }

    private function matchRoute(): void
    {
        foreach($this->routers as $routerName => $router) {
            foreach($router->getRoutes() as $regexPath => $vars) {
                if (preg_match($regexPath, $this->request->getUri())) {
                    $this->controller = new $vars['controller']($this->request);
                    $this->method = $vars['method'];
                    $this->arguments = (new Placeholder($vars['path'], $this->request->getUri()))->getPlaceholders();

                    return;
                }
            }
        }
    }
}