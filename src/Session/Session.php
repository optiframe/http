<?php

declare(strict_types=1);

namespace OptiFrame\Http\Session;

use OptiFrame\Http\Session\Command\SaveSession;
use OptiFrame\Http\Session\Query\DropSession;
use OptiFrame\Http\Session\Query\GetSession;
use OptiFrame\Library\Handler\HandleTrait;
use OptiFrame\Library\Object\Id;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class Session
{
    use HandleTrait;

    private Id $id;
    private array $storage;

    private bool $saveSwitch = false;

    private const DEFAULT_PREFIX = 'session';
    private const DEFAULT_TTL = 2592000;
    private const DEFAULT_REFRESH_TIME = 86400;
    private const EXPIRATION_TIME_FLAG = 'expirationTime';
    private const SESSION_COLLECTION = 'session';
    private const SESSION_SAVES_PROVIDERS = [
        'File' => \OptiFrame\Library\Provider\NoSQL\FileProvider::class,
        'Redis' => \OptiFrame\Library\Provider\NoSQL\RedisProvider::class
    ];

    public function __construct(
        ?Id $id = null,
        private int $ttl = self::DEFAULT_TTL,
        private int $refreshTime = self::DEFAULT_REFRESH_TIME
    ) {
        $this->id = $id ?? new Id(null, self::DEFAULT_PREFIX);
        $this->extractSessionFromMemory();
        $this->checkExpirationTime();
    }

    public function __destruct()
    {
        $this->saveConditionally();
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function get(string $name): null|string|array
    {
        return $this->storage[$name] ?? null;
    }

    public function set(string $name, string|array $value): void
    {   
        $this->storage[$name] = $value;
        $this->saveSwitch = true;
    }

    public function getObject(string $name): null|object
    {
        return unserialize(base64_decode($this->storage[$name])) ?? null;
    }

    public function setObject(string $name, object $value): void
    {   
        $this->storage[$name] = base64_encode(serialize($value));
        $this->saveSwitch = true;
    }

    public function isset(string $name): bool
    {
        return isset($this->storage[$name]);
    }

    public function unset(string $name): void
    {
        unset($this->storage[$name]);
        $this->saveSwitch = true;
    }

    public function saveConditionally(): void
    {
        if ($this->saveSwitch) {
            $this->save();
        }
    }

    public function save(): void
    {
        $this->handle(
            new SaveSession(
                $this->id,
                $this->storage
            ),
            $this->getProvider()
        );
    }

    public function drop(?Id $id = null): void
    {
        $this->handle(
            new DropSession(
                $id ?? $this->id
            ),
            $this->getProvider()
        );
    }

    private function extractSessionFromMemory(): void
    {
        $this->storage = $this->handle(
            new GetSession(
                $this->id
            ),
            $this->getProvider()
        );
    }

    private function checkExpirationTime(): void
    {
        if (!$this->isset[self::EXPIRATION_TIME_FLAG] || ((int) $this->get(self::EXPIRATION_TIME_FLAG) - self::DEFAULT_REFRESH_TIME) < time()) {
            $this->set(self::EXPIRATION_TIME_FLAG, (string) (time() + $this->ttl));
        }
    }

    private function getProvider(): NoSQLProviderInterface
    {
        $provider = self::SESSION_SAVES_PROVIDERS[$_ENV['SESSION_SAVES_PROVIDER']];
        return new $provider(self::SESSION_COLLECTION, null, $this->ttl);
    }
}