<?php

declare(strict_types=1);

namespace OptiFrame\Http\Session\Handler;

use OptiFrame\Http\Session\Query\DropSession;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class DropSessionHandler
{
    public function __construct(
        private NoSQLProviderInterface $provider;
    ) {}

    public function __invoke(DropSession $query): bool
    {
        return $this->provider->deleteObject(
            null,
            $query->getId()
        );
    }
}