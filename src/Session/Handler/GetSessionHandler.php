<?php

declare(strict_types=1);

namespace OptiFrame\Http\Session\Handler;

use OptiFrame\Http\Session\Query\GetSession;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class GetSessionHandler
{
    public function __construct(
        private NoSQLProviderInterface $provider
    ) {}

    public function __invoke(GetSession $query): null|array
    {
        return $this->provider->getObject(
            null,
            $query->getId()
        );
    }
}