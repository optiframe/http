<?php

declare(strict_types=1);

namespace OptiFrame\Http\Session\Handler;

use OptiFrame\Http\Session\Command\SaveSession;
use OptiFrame\Library\Provider\NoSQL\NoSQLProviderInterface;

class SaveSessionHandler
{
    public function __construct(
        private NoSQLProviderInterface $provider
    ) {}

    public function __invoke(SaveSession $command): bool
    {
        return $this->provider->setObject(
            null,
            $command->getId(),
            $command->getStorage()
        );
    }
}