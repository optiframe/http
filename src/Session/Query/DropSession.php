<?php

declare(strict_types=1);

namespace OptiFrame\Http\Session\Query;

use OptiFrame\Library\Handler\GetHandlerTrait;
use OptiFrame\Library\Interface\QueryInterface;
use OptiFrame\Library\Object\Id;

class DropSession implements QueryInterface
{
    use GetHandlerTrait;
    private const HANDLER = \OptiFrame\Http\Session\Handler\DropSessionHandler::class;

    public function __construct(
        private Id $id
    ) {}

    public function getId(): string
    {
        return (string) $this->id;
    }
}