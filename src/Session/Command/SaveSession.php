<?php

declare(strict_types=1);

namespace OptiFrame\Http\Session\Command;

use OptiFrame\Library\Handler\GetHandlerTrait;
use OptiFrame\Library\Interface\CommandInterface;
use OptiFrame\Library\Object\Id;

class SaveSession implements CommandInterface
{
    use GetHandlerTrait;
    private const HANDLER = \OptiFrame\Http\Session\Handler\SaveSessionHandler::class;

    public function __construct(
        private Id $id,
        private array $storage
    ) {}

    public function getId(): string
    {
        return (string) $this->id;
    }

    public function getStorage(): array
    {
        return $this->storage;
    }
}