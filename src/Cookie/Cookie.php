<?php

declare(strict_types=1);

namespace OptiFrame\Http\Cookie;

class Cookie
{
    private string $collection;

    /** Default time in seconds of expires cookies */
    private const DEFAULT_TTL = 2592000; # 2 592 000 seconds = 30 days
    private const DEFAULT_COLLECTION = 'optf';

    public function __construct(
        ?string $collection = null,
        private int $ttl = self::DEFAULT_TTL,
        private string $path = '',
        private string $domain = '',
        private bool $secure = false,
        private bool $httpOnly = false
    ) {
        $this->collection = sprintf('%s_', $collection ?? self::DEFAULT_COLLECTION);
    }
    
    public function get(string $name): ?string
    {
        return $_COOKIE[$this->collection . $name] ?? null;
    }

    public function set(string $name, string $value): bool
    {
        return setcookie(
            $this->collection . $name,
            $value,
            time() + $this->ttl,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httpOnly
        );
    }

    public function isset(string $name): bool
    {
        return isset($_COOKIE[$this->collection . $name]);
    }

    public function unset(string $name): bool
    {
        if (isset($_COOKIE[$this->collection . $name])) {
            unset($_COOKIE[$this->collection . $name]); 
            setcookie(
                $this->collection . $name,
                null,
                -1,
                $this->path,
                $this->domain,
                $this->secure,
                $this->httpOnly
            ); 
            return true;
        }

        return false;
    }

    public function getCollection(): array
    {
        $keys = preg_grep(sprintf('/^%s.+$/', $this->collection), $_COOKIE, 0);
        if (!$keys) {
            return [];
        }
        $output = [];
        $pos = strlen($this->collection);
        foreach ($keys as $key) {
            $output[substr($key, $pos)] = $_COOKIE[$key];
        }

        return $output;
    }

    public function dropCollection(): bool
    {
        $keys = preg_grep(sprintf('/^%s.+$/', $this->collection), $_COOKIE, 0);
        if (!$keys) {
            return false;
        }
        foreach($keys as $key) {
            unset($_COOKIE[$this->collection . $key]); 
            setcookie(
                $this->collection . $key,
                null,
                -1,
                $this->path,
                $this->domain,
                $this->secure,
                $this->httpOnly
            ); 
        }

        return true;
    }
}