<?php

declare(strict_types=1);

namespace OptiFrame\Http\Cookie;

use OptiFrame\Library\Cipher\AES;

class SecretCookie
{
    private AES $cipher;
    private Cookie $provider;
    private array $storage;
    private array $keys = [];

    private const DEFAULT_COLLECTION = 'secret';
    private const DEFAULT_TTL = 2592000;
    private const MAX_COOKIE_SIZE = 512;
    private const MAX_COOKIE_COUNT = 50;

    public function __construct(
        private string $domain,
        private string $path = '',
        private string $prefix = self::DEFAULT_COLLECTION,
        private int $ttl = self::DEFAULT_TTL,
        private bool $secure = true,
        private bool $httpOnly = true
    ) {
        $this->cipher = new AES();
        $this->provider = new Cookie(
            $this->prefix,
            $this->ttl,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httpOnly
        );

        $this->extractSecretCookiesFromGlobals();
    }

    public function set(string $key, string $value): void
    {
        $this->storage[$key] = $value;
    }

    public function get(string $key): ?string
    {
        return $this->storage[$key] ?? null;
    }

    public function isset(string $key): bool
    {
        return isset($this->storage[$key]);
    }

    public function unset(string $key): void
    {
        unset($this->storage[$key]);
    }

    public function save(): void
    {
        if (empty($this->storage)) {
            return;
        }

        $cookie = [];
        foreach($this->storage as $key => $value) {
            if (strlen($key . $value) > self::MAX_COOKIE_SIZE) {
                continue;
            }
            $cookie[$key] = $value;
            if (strlen(json_encode($cookie)) > self::MAX_COOKIE_SIZE) {
                $this->provider->set(array_pop($this->keys) ?? (string) random_int(1000000, 9999999), $this->encode($cookie));
                $cookie = [];
            }
        }
        if (!empty($cookie)) {
            $this->provider->set(array_pop($this->keys) ?? (string) random_int(1000000, 9999999), $this->encode($cookie));
        }
    }

    private function extractSecretCookiesFromGlobals(): void
    {
        $cookies = $this->provider->getCollection();
        foreach ($cookies as $key => $value) {
            $fields = $this->decode($value);
            foreach ($fields as $name => $var) {
                $this->storage[$name] = $var;
            }
            $this->keys[] = $key;
        }
    }

    private function decode(string $cipher): array
    {
        if ($output = json_decode($this->cipher->decrypt($cipher, true), true)) {
            return $output;
        }

        return [];
    }

    private function encode(array $content): string
    {
        return $this->cipher->encrypt(json_encode($content), true);
    }
}
