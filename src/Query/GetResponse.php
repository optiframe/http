<?php

declare(strict_types=1);

namespace OptiFrame\Http\Query;

use OptiFrame\Http\Controller\AbstractController;
use OptiFrame\Http\DTO\Request;
use OptiFrame\Http\Router\Router;
use OptiFrame\Library\Interface\QueryInterface;

class GetResponse implements QueryInterface
{
    private AbstractController $controller;
    private string $method;
    private array $placeholders;

    private const HANDLER = \OptiFrame\Http\Handler\GetResponseHandler::class;

    public function __construct(Request $request)
    {
        list($this->controller, $this->method, $this->placeholders) = (new Router($request))->getResponseArguments();
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getController(): AbstractController
    {
        return $this->controller;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getPlaceholders(): array
    {
        return $this->placeholders;
    }
}