<?php

declare(strict_types=1);

namespace OptiFrame\Http\Query;

use OptiFrame\Library\Handler\GetHandlerTrait;
use OptiFrame\Library\Interface\QueryInterface;

class GetRequest implements QueryInterface
{
    use GetHandlerTrait;
    
    private const HANDLER = \OptiFrame\Http\Handler\GetRequestHandler::class;
}