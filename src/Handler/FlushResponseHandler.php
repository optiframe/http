<?php

declare(strict_types=1);

namespace OptiFrame\Http\Handler;

use OptiFrame\Http\Command\FlushResponse;
use OptiFrame\Http\Provider\FlushProvider;

class FlushResponseHandler
{
    private FlushProvider $provider;

    public function __construct(FlushProvider $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(FlushResponse $command): void
    {
        $this->provider->setHeaders(
            $command->getResponse()->getHeaders()
        );
        $this->provider->setResponseCode(
            $command->getResponse()->getStatus()
        );
        $this->provider->flush(
            $command->getResponse()->getContent()
        );
    }
}