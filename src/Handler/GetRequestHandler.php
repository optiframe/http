<?php

declare(strict_types=1);

namespace OptiFrame\Http\Handler;

use OptiFrame\Http\DTO\Request;
use OptiFrame\Http\Provider\HttpRequestProvider;
use OptiFrame\Http\Query\GetRequest;

class GetRequestHandler
{
    private HttpRequestProvider $provider;

    public function __construct(HttpRequestProvider $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(GetRequest $query): Request
    {
        return $this->provider->getRequest();
    }
}