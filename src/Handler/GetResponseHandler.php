<?php

declare(strict_types=1);

namespace OptiFrame\Http\Handler;

use OptiFrame\Http\DTO\Response;
use OptiFrame\Http\Provider\HttpResponseProvider;
use OptiFrame\Http\Query\GetResponse;

class GetResponseHandler
{
    private HttpResponseProvider $provider;

    public function __construct(HttpResponseProvider $provider)
    {
        $this->provider = $provider;
    }

    public function __invoke(GetResponse $query): Response
    {
        return $this->provider->executeControllerMethod(
            $query->getController(),
            $query->getMethod(),
            $query->getPlaceholders()
        );
    }
}