<?php

declare(strict_types=1);

namespace OptiFrame\Http\Command;

use OptiFrame\Http\DTO\Response;
use OptiFrame\Library\Interface\CommandInterface;

class FlushResponse implements CommandInterface
{
    private Response $response;

    private const HANDLER = \OptiFrame\Http\Handler\FlushResponseHandler::class;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function getHandler(): string
    {
        return self::HANDLER;
    }

    public function getResponse(): Response
    {
        return $this->response;
    }
}