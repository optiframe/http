<?php

declare(strict_types=1);

namespace OptiFrame\Http\Controller;

use OptiFrame\Http\DTO\Request;
use OptiFrame\Http\DTO\Response;

abstract class AbstractController
{
    public function __construct(
        private Request $request
    ) {}

    public function getNotFound($arguments): Response
    {
        return new Response('Not found', 404, []);
    }
}