<?php

declare(strict_types=1);

namespace OptiFrame\Http\Controller;

use OptiFrame\Http\DTO\Request;
use OptiFrame\Http\DTO\Response;

class DefaultController extends AbstractController
{
    public function getWelcome(): Response
    {
        return new Response('works', 200, []);
    }

    public function getTest(): Response
    {
        return new Response('test', 200, []);
    }
}