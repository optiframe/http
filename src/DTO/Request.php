<?php

declare(strict_types=1);

namespace OptiFrame\Http\DTO;

class Request
{
    public function __construct(
        private array $headers,
        private string $method,
        private string $protocol,
        private string $domain,
        private string $host,
        private string $uri,
        private array $query,
        private array $post,
        private array $files,
        private array $cookies,
        private array $acceptableLanguages,
        private array $acceptableEncodings,
        private array $acceptableContentTypes,
        private string $ipAdress,
        private string $userAgent
    ) {}

    public function getAsArray(): array
    {
        return [
            'headers' => $this->headers,
            'method' => $this->method,
            'protocol' => $this->protocol,
            'domain' => $this->domain,
            'host' => $this->host,
            'uri' => $this->uri,
            'query' => $this->query,
            'post' => $this->post,
            'files' => $this->files,
            'cookies' => $this->cookies,
            'acceptableLanguages' => $this->acceptableLanguages,
            'acceptableEncodings' => $this->acceptableEncodings,
            'acceptableContentTypes' => $this->acceptableContentTypes,
            'ipAdress' => $this->ipAdress,
            'userAgent' => $this->userAgent
        ];
    }

    /**
     * Get the value of headers
     *
     * @return  array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Get the value of method
     *
     * @return  string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get the value of protocol
     *
     * @return  string
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * Get the value of domain
     *
     * @return  string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * Get the value of host
     *
     * @return  string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * Get the value of uri
     *
     * @return  string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * Get the value of query
     *
     * @return  array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * Get the value of post
     *
     * @return  array
     */
    public function getPost(): array
    {
        return $this->post;
    }

    /**
     * Get the value of files
     *
     * @return  array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * Get the value of cookies
     *
     * @return  array
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * Get the value of acceptableLanguages
     *
     * @return  array
     */
    public function getAcceptableLanguages(): array
    {
        return $this->acceptableLanguages;
    }

    /**
     * Get the value of acceptableEncodings
     *
     * @return  array
     */
    public function getAcceptableEncodings(): array
    {
        return $this->acceptableEncodings;
    }

    /**
     * Get the value of acceptableContentTypes
     *
     * @return  array
     */
    public function getAcceptableContentTypes(): array
    {
        return $this->acceptableContentTypes;
    }

    /**
     * Get the value of ipAdress
     *
     * @return  string
     */
    public function getIpAdress(): string
    {
        return $this->ipAdress;
    }

    /**
     * Get the value of userAgent
     *
     * @return  string
     */
    public function getUserAgent(): string
    {
        return $this->userAgent;
    }
}