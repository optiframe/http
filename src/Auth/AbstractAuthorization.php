<?php

declare(strict_types=1);

namespace OptiFrame\Http\Auth;

use OptiFrame\Http\Auth\DTO\Token;
use OptiFrame\Http\Auth\DTO\User;
use OptiFrame\Http\Session\Session;

abstract class AbstractAuthorization
{
    private User $user;
    private Session $session;

    public function __construct(
        private Token $token
    ) {}

    public function getUser(): User
    {
        return $this->user;
    }

    public function getSession(): Session
    {
        return $this->session;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

    /** Get data from database and session and check what accesses user has */
    abstract public function authorizeUser(): void;
} 