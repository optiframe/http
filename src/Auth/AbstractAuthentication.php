<?php

declare(strict_types=1);

namespace OptiFrame\Http\Auth;

use OptiFrame\Http\Auth\DTO\Token;
use OptiFrame\Http\Auth\DTO\User;
use OptiFrame\Http\Session\Session;
use OptiFrame\Library\Object\Id;

abstract class AbstractAuthentication
{
    private User $user;
    private Session $session;
    private Token $token;

    public function __construct(
        private Id $id,
        private string $secret
    ) {}

    public function getUser(): User
    {
        return $this->user;
    }

    public function getSession(): Session
    {
        return $this->session;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

    /** Create Token and User */
    abstract public function authenticateUser(): void;

    /** Create Session to store Token if user is authenticated */
    abstract public function startSession(): void;
}