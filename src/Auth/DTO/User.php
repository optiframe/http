<?php

declare(strict_types=1);

namespace OptiFrame\Http\Auth\DTO;

use OptiFrame\Library\Object\Id;

class User
{
    public function __construct(
        private ?Id $id
    ) {}

    public function getId(): ?Id
    {
        return $this->id;
    }

    public function isAuthenticated(): bool
    {
        return $this->id ? true : false;
    }
}