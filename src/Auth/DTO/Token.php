<?php

declare(strict_types=1);

namespace OptiFrame\Http\Auth\DTO;

class Token
{
    public function __construct(
        private string $token
    ) {}

    public function __toString(): string
    {
        return $this->token;
    }
}