<?php

declare(strict_types=1);

namespace OptiFrame\Http;

use OptiFrame\Http\Command\FlushResponse;
use OptiFrame\Http\DTO\Request;
use OptiFrame\Http\DTO\Response;
use OptiFrame\Http\Provider\FlushProvider;
use OptiFrame\Http\Provider\HttpRequestProvider;
use OptiFrame\Http\Provider\HttpResponseProvider;
use OptiFrame\Http\Query\GetRequest;
use OptiFrame\Http\Query\GetResponse;
use OptiFrame\Library\Handler\HandleTrait;
use OptiFrame\Library\Kernel\Extension;
use OptiFrame\Library\Logs\Logs;

class Http extends Extension
{
    use HandleTrait;

    private Response $response;
    private Request $request;

    public function __construct()
    {
        ob_start();

        $this->request = $this->handle(
            new GetRequest(),
            new HttpRequestProvider()
        );
    }

    public function run(): void
    {
        $this->response = $this->handle(
            new GetResponse($this->request),
            new HttpResponseProvider()
        );
    }

    public function terminate(): void
    {
        $this->handle(
            new FlushResponse($this->response),
            new FlushProvider()
        );
        
        fastcgi_finish_request();

        $logs = new Logs();
        $logs->info->set('request', $this->request->getAsArray());
    }
}