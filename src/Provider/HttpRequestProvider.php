<?php

declare(strict_types=1);

namespace OptiFrame\Http\Provider;

use OptiFrame\Http\DTO\Request;
use OptiFrame\Library\Interface\ProviderInterface;

class HttpRequestProvider implements ProviderInterface
{
    public function getRequest(): Request
    {
        return new Request(
            $this->getHeadersFromGlobals(),
            $this->getMethodFromGlobals(),
            $this->getProtocolFromGlobals(),
            $this->getDomainFromGlobals(),
            $this->getHostFromGlobals(),
            $this->getUriFromGlobals(),
            $this->getQueryFromGlobals(),
            $this->getPostFromGlobals(),
            $this->getFilesFromGlobals(),
            $this->getCookiesFromGlobals(),
            $this->getAcceptableLanguagesFromGlobals(),
            $this->getAcceptableEncodingsFromGlobals(),
            $this->getAcceptableContentTypesFromGlobals(),
            $this->getIpAdressFromGlobals(),
            $this->getUserAgentFromGlobals()
        );
    }

    private function getHeadersFromGlobals(): array
    {
        return getallheaders();
    }

    private function getMethodFromGlobals(): string
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    private function getProtocolFromGlobals(): string
    {
        return array_key_exists('HTTPS', $_SERVER) ? 'https' : ($_SERVER['REQUEST_SCHEME'] ?? 'http');
    }

    private function getDomainFromGlobals(): string
    {
        $host = explode('.', $this->getHostFromGlobals());
        return $host[array_key_last($host) - 1] . '.' . $host[array_key_last($host)];
    }

    private function getHostFromGlobals(): string
    {
        return $_SERVER['HTTP_HOST'];
    }

    private function getUriFromGlobals(): string
    {
        $uri = $_SERVER['REQUEST_URI'] ?? '';
        $pos = strpos($uri, '?');
        if ($pos !== false) {
            $uri = substr($uri, 0, $pos);
        }
        if (substr($uri,-1) == '/' && strlen($uri) > 1) {
            return substr($uri, 0, -1);
        }
        
        return $uri;
    }

    private function getQueryFromGlobals(): array
    {
        return $_GET;
    }

    private function getPostFromGlobals(): array
    {
        return $_POST;
    }

    private function getFilesFromGlobals(): array
    {
        return $_FILES;
    }

    private function getCookiesFromGlobals(): array
    {
        return $_COOKIE;
    }

    private function getAcceptableLanguagesFromGlobals(): array
    {
        return array_map(function($callback){
            if (strpos($callback,';') !== false) {
                return explode(';',$callback)[0];
            }
            return $callback;
        }, explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']));
    }

    private function getAcceptableEncodingsFromGlobals(): array
    {
        return explode(', ', $_SERVER['HTTP_ACCEPT_ENCODING']);
    }

    private function getAcceptableContentTypesFromGlobals(): array
    {
        return array_map(function($callback){
            if (strpos($callback,';') !== false) {
                return explode(';',$callback)[0];
            }
            return $callback;
        }, explode(',', $_SERVER['HTTP_ACCEPT']));
    }

    private function getIpAdressFromGlobals(): string
    {
        return $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['HTTP_X_FORWARDER_FOR'] ?? $_SERVER['REMOTE_ADDR'] ?? '';
    }

    private function getUserAgentFromGlobals(): string
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

}