<?php

declare(strict_types=1);

namespace OptiFrame\Http\Provider;

use OptiFrame\Http\Controller\AbstractController;
use OptiFrame\Http\DTO\Response;
use OptiFrame\Library\Interface\ProviderInterface;

class HttpResponseProvider implements ProviderInterface
{
    public function executeControllerMethod(AbstractController $controller, string $method, array $placeholders): Response
    {
        return $controller->{$method}($placeholders);
    }
}