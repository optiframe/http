<?php

declare(strict_types=1);

namespace OptiFrame\Http\Provider;

use OptiFrame\Library\Interface\ProviderInterface;

class FlushProvider implements ProviderInterface
{
    public function setHeaders(array $headers): void
    {
        foreach ($headers as $key => $value) {
            header(sprintf('%s: %s', (string) $key, (string) $value));
        }
    }

    public function setResponseCode(int $status): void
    {
        http_response_code($status);
    }

    public function flush(string $content): void
    {
        echo $content;
    }
}